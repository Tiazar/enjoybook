class AddCreatedAtIndexToRates < ActiveRecord::Migration[5.2]
  def change
    add_index :rates, :created_at
  end
end
