class CreateRates < ActiveRecord::Migration[5.2]
  def change
    create_table :rates do |t|
      t.string :name
      t.float :buy
      t.float :sell

      t.timestamps
    end
    add_index :rates, :name
  end
end
