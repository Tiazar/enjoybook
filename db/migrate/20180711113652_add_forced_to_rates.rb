class AddForcedToRates < ActiveRecord::Migration[5.2]
  def change
    add_column :rates, :forced, :datetime, null: true
  end
end
