# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
12.times do |t|
  Rate.create(name: "USD", sell: rand(35.00...65.00).round(2), buy: rand(35.00...65.00).round(2), created_at:
      DateTime.now.in_time_zone(Time.zone).beginning_of_day + t.hour
  )
  Rate.create(name: "EUR", sell: rand(50.00...90.00).round(2), buy: rand(50.00...90.00).round(2), created_at:
      DateTime.now.in_time_zone(Time.zone).beginning_of_day + t.hour
  )
end