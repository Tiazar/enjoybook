Rails.application.routes.draw do
  root 'charts#index'

  get 'admin', to: 'charts#new'
  post 'admin', to: 'charts#create'
end
