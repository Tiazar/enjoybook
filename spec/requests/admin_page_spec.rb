require 'spec_helper'


describe "Admin page" do

  subject { page }

  before { visit admin_path }

  describe "admin page" do

    it { should have_content('Введите форсированный курс') }
    it { should have_content('Покупка') }
    it { should have_content('Дата и время') }

    it "should have fields" do
      page.should have_css('.rate_buy')
      page.should have_css('.rate_sell')
      page.should have_css('.rate_forced')
      page.should have_css('.rate_name', visible: false)
    end

  end

  describe "with valid information" do
    before do
      fill_in "rate_forced",           with: '19.07.2018 1:50'
      fill_in "rate_sell",             with: 99.99
      fill_in "rate_buy",              with: 99.99
      find('input[name="commit"]').click
    end

    it { should have_selector('.alert-success') }
    it {
      expect(find('.avg_usd_sell p').text).to eq 'Продажа: 99.99'
      expect(find('.avg_usd_buy p').text).to eq 'Покупка: 99.99'
    }
  end

end