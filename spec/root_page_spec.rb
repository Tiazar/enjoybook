require 'spec_helper'

describe "Root page" do

  subject { page }

  describe "rates page" do

    12.times do |t|
      Rate.create(name: "USD", sell: 50.64, buy: 60.35, created_at:
          DateTime.now.in_time_zone(Time.zone).beginning_of_day + t.hour
      )
    end

    before { visit root_path }

    it "should have USD" do
      page.should have_css('.avg_usd_sell')
      page.should have_css('.avg_usd_buy')

      expect(page.find('.avg_usd_sell p').text).to eq 'Продажа: 50.64'
      expect(page.find('.avg_usd_buy p').text).to eq 'Покупка: 60.35'
    end
  end

  describe "tinkoff rates api" do
    it "should respond" do
      require 'httparty'
      response = HTTParty.get("https://www.tinkoff.ru/api/v1/currency_rates/")

      json = JSON.parse(response.body)

      expect(response).to be_success
      buy = json['payload']['rates'].select { |r| r['category'] == 'DepositPayments' && (r['fromCurrency']['name'] == "USD" ) && r['toCurrency']['name'] == 'RUB' }.first['buy']
      expect(buy).to be_a(Float)
      sell = json['payload']['rates'].select { |r| r['category'] == 'DepositPayments' && (r['fromCurrency']['name'] == "USD" ) && r['toCurrency']['name'] == 'RUB' }.first['buy']
      expect(sell).to be_a(Float)
    end
  end
end