require 'spec_helper'

describe Rate do
  before { @rate = Rate.new(
      name: "USD",
      sell: 99.99,
      buy: 99.99,
      forced: '19.07.2018 1:50')
  }

  subject { @rate }

  it { should respond_to(:name) }
  it { should respond_to(:sell) }
  it { should respond_to(:buy) }
  it { should respond_to(:forced) }

  it { should be_valid }

  describe "when buy not valid" do
    before { @rate.buy = " "}
    it { should_not be_valid}
  end

  describe "when sell not valid" do
    before { @rate.sell = " "}
    it { should_not be_valid}
  end

  describe "when name not valid" do
    before { @rate.name = " "}
    it { should_not be_valid}
  end

  describe "rate saving" do
    before {
      @count = Rate.count
      @rate.save
    }
    it "should be count +1" do
      expect(Rate.count).to eq @count + 1
    end
  end
end