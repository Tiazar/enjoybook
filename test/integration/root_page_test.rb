require 'test_helper'

require 'spec_helper'

describe "Root page" do

  subject { page }

  describe "rates page" do

    12.times do |t|
      Rate.create(name: "USD", sell: 50.64, buy: 60.35, created_at:
          DateTime.now.in_time_zone(Time.zone).beginning_of_day + t.hour
      )
      Rate.create(name: "EUR", sell: 50.64, buy: 60.35, created_at:
          DateTime.now.in_time_zone(Time.zone).beginning_of_day + t.hour
      )
    end

    before { visit root_path }

    it "should have USD" do
      page.should have_css('.avg_usd_sell')
      page.should have_css('.avg_usd_buy')
    end
  end
end