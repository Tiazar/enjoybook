class ChartsController < ApplicationController

  def index
    rates = Rate.where(name: 'USD').order(created_at: :desc).limit(2)
    @last_usd = rates.first
    @previus_usd = rates.last
    forced_usd = Rate.where(name: 'USD').where('forced >= ?', DateTime.now).order(forced: :desc).first
    if forced_usd
      @last_usd = forced_usd
    end
  end

  def new
    @rate = Rate.where(name: 'USD').where('forced >= ?', DateTime.now).order(forced: :desc).first || Rate.new
  end

  def create
    @rate = Rate.new(rate_params)
    if @rate.save!
      flash[:success] = "Форсированный курс добавлен"
      redirect_to root_path
    end
  end

  private

  def rate_params
    params.require(:rate).permit(:name, :sell, :buy, :forced)
  end
end
