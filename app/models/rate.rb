class Rate < ApplicationRecord
  validates :name, :buy, :sell, presence: true
  validates :buy, :sell, numericality: true
  validate do
    if self.forced
      self.errors[:forced] << "must be a valid date" unless (DateTime.parse(self.forced.to_s) rescue false)
    end
  end
end
