class SimpleJob
  @queue = :simple

  def self.perform
    require 'httparty'
    response = HTTParty.get("https://www.tinkoff.ru/api/v1/currency_rates/")
    if response.success?
      if @json = JSON.parse(response.body)
        @json = @json['payload']['rates'].select { |r| r['category'] == 'DepositPayments' && (r['fromCurrency']['name'] == "USD" ||
            r['fromCurrency']['name'] == "EUR" ) && r['toCurrency']['name'] == 'RUB' }
        @json.each do |j|
          Rate.create!(name: j['fromCurrency']['name'], buy: j['buy'], sell: j['sell'])
        end
      end
    end
  end
end